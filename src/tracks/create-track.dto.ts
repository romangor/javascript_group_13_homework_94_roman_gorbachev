export class CreateTrackDto {
  album: string;
  title: string;
  lengthOfTrack: string;
  youtubeLink: string;
  is_published: boolean;
}
