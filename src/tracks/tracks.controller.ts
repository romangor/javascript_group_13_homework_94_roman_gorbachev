import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Track, TrackDocument } from '../schemas/track.schema';
import { CreateTrackDto } from './create-track.dto';

@Controller('tracks')
export class TracksController {
  constructor(
    @InjectModel(Track.name)
    private trackModel: Model<TrackDocument>,
  ) {}
  @Get()
  getTracks(@Query('album') album: string) {
    if (album) {
      return this.trackModel.find({ album: album });
    }
    return this.trackModel.find();
  }
  @Post()
  create(@Body() trackData: CreateTrackDto) {
    const link = trackData.youtubeLink.replace(
      'https://www.youtube.com/watch?v=',
      '',
    );
    const track = new this.trackModel({
      album: trackData.album,
      title: trackData.title,
      lengthOfTrack: trackData.lengthOfTrack,
      youtubeLink: link,
      is_published: false,
    });

    return track.save();
  }
  @Delete(':id')
  removeTrack(@Param('id') id: string) {
    return this.trackModel.deleteOne({ _id: id });
  }
}
