import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type ArtistDocument = Artist & Document;

@Schema()
export class Artist {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  information: string;

  @Prop({ required: true })
  photo: string;

  @Prop()
  is_published: boolean;
}
export const ArtistSchema = SchemaFactory.createForClass(Artist);
