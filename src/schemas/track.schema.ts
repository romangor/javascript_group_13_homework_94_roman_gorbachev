import mongoose, { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type TrackDocument = Track & Document;

@Schema()
export class Track {
  @Prop({ ref: 'Album', required: true })
  album: mongoose.Schema.Types.ObjectId;

  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  lengthOfTrack: string;

  @Prop()
  youtubeLink: string;

  @Prop()
  is_published: boolean;
}

export const TrackSchema = SchemaFactory.createForClass(Track);
