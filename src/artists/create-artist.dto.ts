export class CreateArtistDto {
  name: string;
  information: string;
  photo: string;
  is_published: boolean;
}
