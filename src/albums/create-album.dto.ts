export class CreateAlbumDto {
  artist: string;
  title: string;
  date: string;
  image: string;
  is_published: boolean;
}
