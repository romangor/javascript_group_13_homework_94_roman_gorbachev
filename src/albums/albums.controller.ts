import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Album, AlbumDocument } from '../schemas/album.schema';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreateAlbumDto } from './create-album.dto';

@Controller('albums')
export class AlbumsController {
  constructor(
    @InjectModel(Album.name)
    private albumModel: Model<AlbumDocument>,
  ) {}
  @Get()
  getAlbumsByArtist(@Query('artist') artist: string) {
    if (artist) {
      return this.albumModel.find({ artist: artist });
    }
    return this.albumModel.find();
  }
  @Get(':id')
  getAlbum(@Param('id') id: string) {
    return this.albumModel.findById(id);
  }
  @Post()
  @UseInterceptors(
    FileInterceptor('image', { dest: './public/uploads/albums' }),
  )
  create(
    @UploadedFile() file: Express.Multer.File,
    @Body() albumData: CreateAlbumDto,
  ) {
    const date = new Date().toString();
    const album = new this.albumModel({
      artist: albumData.artist,
      title: albumData.title,
      date: date,
      image: file ? 'uploads/albums/' + file.filename : null,
      is_published: false,
    });
    return album.save();
  }
  @Delete(':id')
  removeAlbum(@Param('id') id: string) {
    return this.albumModel.deleteOne({ _id: id });
  }
}
